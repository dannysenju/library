package edu.ucc.library.controller;

import edu.ucc.library.domain.Book;
import edu.ucc.library.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("books-search-stock")
    public ResponseEntity<Book> searchInStock(@RequestParam("author") String author, @RequestParam("title") String title) {
        Book book = this.bookService.findByAuthorAndTitleAndInStock(author, title);

        if (book.getId() != null)
            return new ResponseEntity<>(book, HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @GetMapping("books-search")
    public ResponseEntity<Book> search(@RequestParam("author") String author, @RequestParam("title") String title) {
        Book book = this.bookService.findByAuthorAndTitle(author, title);

        if (book.getId() != null)
            return new ResponseEntity<>(book, HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @GetMapping("books")
    public ResponseEntity<List<Book>> findAll() {
        List<Book> bookList = this.bookService.findAll();

        if (!bookList.isEmpty())
            return new ResponseEntity<>(bookList, HttpStatus.OK);
        else
            return new ResponseEntity<>(bookList, HttpStatus.NOT_FOUND);
    }

    @GetMapping("books/{id}")
    public ResponseEntity<Book> findById(@PathVariable("id") Long id) {
        Book byId = this.bookService.findById(id);

        if (byId.getId() != null)
            return new ResponseEntity<>(byId, HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @PostMapping("book")
    public ResponseEntity<Book> create(@Valid @RequestBody Book book) {
        Book save = this.bookService.save(book);

        if (save.getId() != null)
            return new ResponseEntity<>(save, HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @PutMapping("book/{id}")
    public ResponseEntity<Book> update(@Valid @RequestBody Book book, @PathVariable("id") Long id) {
        Book update = this.bookService.update(book, id);

        if (update != null)
            return new ResponseEntity<>(update, HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("book/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        boolean delete = this.bookService.delete(id);

        if (delete)
            return new ResponseEntity<>("eliminado", HttpStatus.OK);
        else
            return new ResponseEntity<>("Fallo Eliminación", HttpStatus.NOT_FOUND);
    }
}
