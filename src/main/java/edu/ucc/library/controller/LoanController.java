package edu.ucc.library.controller;

import edu.ucc.library.domain.Loan;
import edu.ucc.library.service.LoanService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class LoanController {
    private final LoanService loanService;

    public LoanController(LoanService loanService) {
        this.loanService = loanService;
    }

    @GetMapping("loans")
    public ResponseEntity<List<Loan>> findAll() {
        return new ResponseEntity<>(this.loanService.findAll(), HttpStatus.OK);
    }

    // TODO probar en postman
    @GetMapping("loans-search")
    public ResponseEntity<List<Loan>> loansSearch(@RequestParam("bookId") Long bookId) {
        return new ResponseEntity<>(this.loanService.findAllByBookId(bookId), HttpStatus.OK);
    }

    // TODO probar en postman
    @PostMapping("loan")
    public ResponseEntity<Loan> create(@Valid @RequestBody Loan loan) {
        try {
            Loan loanCreate = this.loanService.create(loan);

            if (loanCreate.getId() != null)
                return new ResponseEntity<>(loanCreate, HttpStatus.OK);
            else
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }
}
