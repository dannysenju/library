package edu.ucc.library.domain;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Entity
@Table(name = "book", schema = "library", uniqueConstraints = {
        @UniqueConstraint(name = "unique_book_in_stock", columnNames = {"title", "author", "volume"})
})
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "title", nullable = false, length = 150)
    private String title;

    @Column(name = "volume", nullable = false, length = 5)
    private String volume;

    @Column(name = "author", nullable = false, length = 50)
    private String author;

    @Column(name = "in_stock", nullable = false, length = 50)
    @Min(value = 0, message = "No quedan más unidades en stock")// NO PUEDE SER NEGATIVO
    private Integer inStock;

    @ManyToOne
    @JoinColumn(name = "editorial_id", referencedColumnName = "id", updatable = false, insertable = false)
    Editorial editorial;

    @Column(name = "editorial_id")
    private Long editorialId;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public Editorial getEditorial() {
        return editorial;
    }

    public void setEditorial(Editorial editorial) {
        this.editorial = editorial;
    }

    public Long getEditorialId() {
        return editorialId;
    }

    public void setEditorialId(Long editorialId) {
        this.editorialId = editorialId;
    }

    public Integer getInStock() {
        return inStock;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }
}
