package edu.ucc.library.domain;

import edu.ucc.library.enums.LoanStateEnum;

import javax.persistence.*;
import javax.validation.constraints.Max;
import java.util.Date;

@Entity
@Table(name = "loan", schema = "library")
public class Loan {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", updatable = false, insertable = false)
    User user;

    @Column(name = "user_id")
    private Long userId;

    @ManyToOne
    @JoinColumn(name = "book_id", referencedColumnName = "id", updatable = false, insertable = false)
    Book book;

    @Column(name = "book_id")
    private Long bookId;

    @Column(name = "request_date")
    private Date requestDate;

    @Column(name = "dead_line")
    private Date deadLine;

    @Column(name = "state", nullable = false)
    @Enumerated(EnumType.STRING)
    private LoanStateEnum loanStateEnum;

    @Column(name = "queue_wait")
    @Max(value = 5, message = "La cola de espera ya esta llena")
    private Long queueWait;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(Date deadLine) {
        this.deadLine = deadLine;
    }

    public LoanStateEnum getLoanStateEnum() {
        return loanStateEnum;
    }

    public void setLoanStateEnum(LoanStateEnum loanStateEnum) {
        this.loanStateEnum = loanStateEnum;
    }

    public Long getQueueWait() {
        return queueWait;
    }

    public void setQueueWait(Long queueWait) {
        this.queueWait = queueWait;
    }
}
