package edu.ucc.library.enums;

public enum LoanStateEnum {
    ACTIVE, EXPIRY, RETURNED, POSTPONE, PENDING
}
