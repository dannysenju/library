package edu.ucc.library.enums;

public enum UserRolEnum {
    ADMIN, LIBRARIAN, CUSTOMER
}
