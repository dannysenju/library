package edu.ucc.library.repository;

import edu.ucc.library.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    // metodos funcionales -> que tengan utilidad
    // Yo como BB quiero listar todos los libros que tengo en la biblioteca
    // 1 Query creation from method names
    List<Book> findAll(); // optimzación realizada springboot

    // JPQL
    // 2 Query by jpql
    @Query(value = "select b from Book b")
    List<Book> getAllBooksByJpql();

    // 3 Query native
    @Query(value = "select b.* from book b", nativeQuery = true)
    List<Book> getAllBooksNativeQuery();

    List<Book> findAllByAuthor(String author);

    @Query(value = "select b from Book b where b.author = :author and b.inStock > 0 and b.title = :title")
    Optional<Book> findByAuthorAndTitleAndInStock(@Param("author") String author, @Param("title") String title);

    @Query(value = "select b from Book b where b.author = :author and b.title = :title")
    Optional<Book> findByAuthorAndTitle(@Param("author") String author, @Param("title") String title);

    @Query(value = "select b.* from book b where b.author = :x", nativeQuery = true)
    List<Book> findAllByAuthorNativeQuery(@Param("x") String author);

    List<Book> findAllByAuthorOrderById(String author); // order by id asc

    List<Book> findAllByAuthorOrderByIdDesc(String author); // order by id desc

    List<Book> findAllByAuthorAndEditorialIdOrderById(String author, Long editorialId); // author and editorial id

    // CRUD -> Create Update y el Delete

}
