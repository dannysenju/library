package edu.ucc.library.repository;

import edu.ucc.library.domain.Loan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Long> {

    @Query(value = "select l from Loan l where l.bookId = :bookId")
    List<Loan> findAllByBookId(@Param("bookId") Long bookId);
}
