package edu.ucc.library.service;

import edu.ucc.library.domain.Book;

import java.util.List;

public interface BookService {
    Book findByAuthorAndTitleAndInStock(String author, String title);
    Book findByAuthorAndTitle(String author, String title);
    List<Book> findAll();
    Book findById(Long id);
    Book save(Book book);
    Book update(Book book, Long id);
    Boolean delete(Long id);
}
