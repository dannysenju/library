package edu.ucc.library.service;

import edu.ucc.library.domain.Book;
import edu.ucc.library.repository.BookRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Book findByAuthorAndTitleAndInStock(String author, String title) {
        Optional<Book> optionalBook = this.bookRepository.findByAuthorAndTitleAndInStock(author, title);

        return optionalBook.orElseGet(Book::new);
//        if (optionalBook.isPresent())
//            return optionalBook.get();
//        else
//            return new Book();
    }

    @Override
    public Book findByAuthorAndTitle(String author, String title) {
        Optional<Book> optionalBook = this.bookRepository.findByAuthorAndTitle(author, title);

        return optionalBook.orElseGet(Book::new);
    }

    @Override
    public List<Book> findAll() {
        return this.bookRepository.findAll();
    }

    @Override
    public Book findById(Long id) {
//        Optional<Book> bookOptional = this.bookRepository.findById(id);
        return this.bookRepository.findById(id).orElseGet(Book::new);
    }

    @Override
    public Book save(Book book) {
        return this.bookRepository.save(book);
    }

    @Override
    public Book update(Book book, Long id) {
        if (this.bookRepository.existsById(id))
            return this.bookRepository.save(book);

        return null;
    }

    @Override
    public Boolean delete(Long id) {
        if (this.bookRepository.existsById(id)) {
            this.bookRepository.deleteById(id);
            return true;
        }

        return false;
    }
}
