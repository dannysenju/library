package edu.ucc.library.service;

import edu.ucc.library.domain.Loan;

import java.util.List;

public interface LoanService {
    Loan create(Loan loan) throws Exception;
    List<Loan> findAll();
    List<Loan> findAllByBookId(Long bookId);
}
