package edu.ucc.library.service;

import edu.ucc.library.domain.Loan;
import edu.ucc.library.enums.LoanStateEnum;
import edu.ucc.library.repository.LoanRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class LoanServiceImpl implements LoanService {
    private final LoanRepository loanRepository;

    public LoanServiceImpl(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    @Override
    public Loan create(Loan loan) throws Exception {
        LocalDate localDate = LocalDate.now();

        loan.setLoanStateEnum(LoanStateEnum.ACTIVE);
        loan.setRequestDate(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
        loan.setDeadLine(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).plusDays(5).toInstant()));

        if (loan.getQueueWait() != null && (loan.getQueueWait() < 0 || loan.getQueueWait() >= 5)) {
            // -1 , -2 6, 7, 8
            // ERROR
            throw new Exception("La cola esta llena");
        }

        return this.loanRepository.save(loan);
    }

    @Override
    public List<Loan> findAll() {
        return this.loanRepository.findAll();
    }

    @Override
    public List<Loan> findAllByBookId(Long bookId) {
        return this.loanRepository.findAllByBookId(bookId);
    }
}
